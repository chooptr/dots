export PATH="$(du $HOME/bin/ | cut -f2 | tr '\n' ':')$PATH"

export PATH="/home/chooptr/etc/cargo/bin:$PATH"

export CONFIG_HOME="$HOME/.config"

export CARGO_HOME="$HOME/etc/cargo"
export RUSTUP_HOME="$HOME/etc/rustup"
#export OPENSSL_INCLUDE_DIR="/usr/include/openssl-1.0"
#export OPENSSL_LIB_DIR="/usr/lib/openssl-1.0"

export ZDOTDIR="$HOME/.config/zsh"

