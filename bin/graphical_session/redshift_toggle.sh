#!/bin/bash

if [ $1 = "on" ];
then
	redshift -P -O 4500
	echo "%{A1:polybar-msg hook redshift 1:}%{F#FFB300}%{F-}%{A}"
else
	redshift -x
	echo "%{A1:polybar-msg hook redshift 2:}%{A}"
fi
