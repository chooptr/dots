#!/usr/bin/bash

# Settings
light -N 5
setxkbmap -option compose:"ralt"
[[ -f $HOME/.Xresources ]] && xrdb $HOME/.Xresources
xinput set-prop "ETPS/2 Elantech Touchpad" "libinput Tapping Enabled" 1

# Daemons
killall -q sxhkd;     sxhkd &
killall -q greenclip; greenclip daemon &
killall -q unclutter; unclutter -noevents &
killall -q compton;   compton --config ~/.config/compton/compton.conf &
killall -q xidlehook; xidlehook --timer normal 3600 'betterlockscreen -l blur' '' &

# Tray
killall -q udiskie;   udiskie --smart-tray &
killall -q nm-applet; nm-applet &
killall -q pasystray; pasystray &
killall -q blueman-applet; blueman-applet &

# Scripts
killall -q dynamic_wall.sh; dynamic_wall.sh &
monitors.sh &
polybarstart.sh &

# Java apps
wmname LG3D
