#!/bin/bash

SCRIPT_FILE="$HOME/scripts/$1"

if [[ -f $SCRIPT_FILE ]]; then
	echo File already exists
else
	echo "#!/bin/bash" > $SCRIPT_FILE
	chmod +x $SCRIPT_FILE
	vim $SCRIPT_FILE
fi
