#!/usr/bin/sh

id=$(wmctrl -lx | grep scratchpad | awk '{print $1}')
if [[ $id ]]
then
	bspc node $id --flag hidden
   	bspc node -f $id
else
    	alacritty --class scratchpad
fi
