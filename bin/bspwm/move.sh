#!/usr/bin/bash


case $2 in
	'west')
		i=-20
		j=0
		;;
	'south')
		i=0 
		j=20
		;;
	'north')
		i=0 
		j=-20
		;;
	'east')
		i=20
		j=0
		;;
esac
bspc node -v $i $j || bspc node -$1 $2

